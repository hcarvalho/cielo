#Projeto Automação Cielo CEP
#Ferramentas/Tecnologias utilizadas:
 - Eclipse 
 - Cucumber
 - Gherkin
 - Maven
 - JUnit

#Sobre o teste:
O cenário pede uma validação de CEP via API, programei apenas um caso de teste parametrizando um CEP válido e inválido 

#Para executar o teste:
1. Clone o projeto via BitBucket;
2. Importe o Maven Project no eclipse;
3. Clique com o botão direito na classe CieloRunner, e então Run As > JUnit;
4. Verifique os resultados no  Console;

#Reports:
 - Um report em html pode ser encontrado em Cielo/target/cucumber/Report.html/index.html
